<?php

namespace ABellver;


use Codedungeon\PHPCliColors\Color;

class BitCoinValue{

    private $prize;
    private $currency;
    private $readOn;
    private $advise;

    public function __construct( float $prize, string $currency, string $readOn){

        $this->prize = $prize;
        $this->currency = $currency;
        $this->readOn = $readOn;

    }

    public function  __toString(){

        if ($this->prize <= 6000 ){

            $this->advise = "invresion no aconsejable";
            return  Color::RED . "$this->currency - $this->prize - $this->advise\n" ;

        } else{

            $this->advise = "invresion aconsejable";
            return  Color::GREEN . "$this->currency - $this->prize - $this->advise\n" ;

        }


    }
}